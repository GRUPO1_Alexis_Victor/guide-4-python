-- Create a new database called 'mainDataBase' -- Connect to the 
'master' database to run this snippet USE master GO -- Create the new 
database if it does not exist already IF NOT EXISTS (
	SELECT [name] FROM sys.databases WHERE [name] = N'mainDataBase' 
) CREATE DATABASE mainDataBase GO USE mainDataBase GO -- create schemas 
CREATE SCHEMA webSocket; go -- create tables CREATE TABLE 
webSocket.sensor (
	entryDate DATE NOT NULL, sensorID INT IDENTITY (1, 1) PRIMARY 
	KEY, sampleTime DATETIME NOT NULL, temperature DECIMAL (10, 2) 
	NOT NULL, humidity DECIMAL (10, 0) NOT NULL
);
SELECT * FROM webSocket.sensor;
