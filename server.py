import socket
import threading
import pyodbc
import pandas as pd
import datetime

###

HEADER = 64
PORT = 3074
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

###

database = 'mainDataBase'
username = 'sa'
password = 'Admin.123'
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+SERVER+';DATABASE='+database+';UID='+username+';PWD='+password)
cursor = cnxn.cursor()

###

def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected")
    connected = True
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)
        if msg_length:
            msg_length = int(msg_length)
            msg = conn.recv(msg_length).decode(FORMAT)
            allData=[]
            for info in msg.split(','):
                allData.append(info)
            instruction="INSERT INTO webSocet.sensor (entryDate, sensorID, sampleTime, temperature, humidity) VALUES ('"+datetime.datetime.now().strftime('%c')+"',"+str(allData[0])+","+str(allData[1])+","+str(allData[2])+","+str(allData[3])+")"
            cursor.execute(instruction)
            cnxn.commit()
            print(f"[{addr}] {msg}")
            conn.send("Msg received".encode(FORMAT))
            if msg == DISCONNECT_MESSAGE:
                conn.send("Disconnecting...".encode(FORMAT))
                connected = False
    conn.close()

def start():
    server.listen()
    print(f"[LISTEN] Server is listening on address {ADDR}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"\n[ACTIVE CONNECTIONS] {threading.activeCount() - 1}")

###

print("[STARTING] server is running.....")
start()


