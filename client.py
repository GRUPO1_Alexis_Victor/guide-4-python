import socket

HEADER = 64
PORT = 3074
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

def send(msg):
    message=msg.encode(FORMAT)

    msg_length = len(message)
    send_length = str(msg_length).encode(FORMAT)

    send_length += b' '*(HEADER-len(send_length))

    client.send(send_length)
    client.send(message)
    print(client.recv(2048).decode(FORMAT))

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)

input()
send('01,10:29:59,23,43')
input()
send('01,10:23:04,24,40')
input()
send('01,10:23:09,26,34')
send(DISCONNECT_MESSAGE)


